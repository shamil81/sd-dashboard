/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 30/12/15
 */
angular.module('sd.partner',['ui.router','ngMaterial','ngSanitize'])
    .config(function ($stateProvider) {



          $stateProvider
         .state('partner', {
             url:'/partner',
             views:{
                 'main':{
                     templateUrl:'partner/partner.tpl.html'
                 }
             },
             data:{
                 pageTitle:'Партнеры'
             }



         })
              .state('partner.detail', {
                  url:'/:id',
                  templateUrl:'partner/partnerDetail.tpl.html'
              });
    })
    .controller('PartnerCtrl', function($scope, $http,$mdToast){

        $scope.partners = [];
        $scope.partner = null;
        $scope.success = false;
        $scope.userAlreadyExists = false;


        var prevEmail = '';


        $scope.showSaveToast = function() {

            $scope.$emit('showToast', {action:'save'});


        };

        $http.get('/api/auth?userType=1')
        .then(function(res){
            var users = res.data;
            users.forEach(function(partner){
                console.dir(partner);
                console.log(partner['passportDate']);


                if(partner.hasOwnProperty('user')){
                    angular.extend(partner, partner.user);
                    delete partner.user;

                }
            });

            users = _.sortBy(users, function(o){
                return o.id;
            });
            $scope.partners = users;

            console.dir($scope.partners);

        });

        $scope.selectUser = function(partner){


            if(partner.isLegalEntity){
                $http.get('/api/partner?user=' + partner.id)
                    .then(function(res){
                        delete res.data[0].user;
                        delete res.data[0].id;
                        angular.extend(partner, res.data[0]);
                        console.dir(res);
                        prevEmail = partner.email;
                        $scope.partner = partner;


                    });
            }else{


                console.log(partner.birthDate);

                   if((partner.birthDate instanceof Date) === false){


                      partner.birthDate = new Date(partner.birthDate.toString().substr(0,10));
                      partner.passportDate = moment(partner.passportDate, 'YYYY-MM-DD').toDate();
                      /* partner.birthDate = moment.utc(partner.birthDate.substr(0,10)).toDate();
                       partner.passportDate = moment.utc(partner.passportDate.substr(0,10)).toDate();*/



                   }
                console.log(partner.birthDate);






                    prevEmail = partner.email;
                    $scope.partner = partner;
            }


        };

        $scope.reset = function(){

            $scope.partner = null;
        };


        $scope.submit = function(frm){
            $scope.success = false;
            $scope.error500 = '';



            var partner = {};

            if(frm.$invalid){
                return;
            }


            angular.copy($scope.partner, partner);

            if($scope.partner.hasOwnProperty('birthDate')){


                console.log($scope.partner.birthDate);
                var birthDate = $scope.partner.birthDate.getFullYear() + '-' + ($scope.partner.birthDate.getMonth() + 1) + '-' + $scope.partner.birthDate.getDate();
                var passportDate = $scope.partner.passportDate.getFullYear() + '-' +  ($scope.partner.passportDate.getMonth() + 1) + '-' + $scope.partner.passportDate.getDate();

                console.log(birthDate);
                partner.birthDate = birthDate;
                partner.passportDate = passportDate;

            }
            if($scope.partner.hasOwnProperty('id')){
                partner.user = $scope.partner.id;

                if(prevEmail !== $scope.partner.email){
                    $scope.partner.confirmed = false;
                }

                console.dir(partner);


                $http.put('/api/partner/' + $scope.partner.id, partner)
                .then(function(res){
                    console.dir(res);
                    $scope.showSaveToast();

                    




                },function(err){
                    

                });
                return;
            }


            console.dir($scope.partner);

            $http.post('/api/partner', partner)
            .then(function(res){
                console.dir(res);
                $scope.success = true;
                $scope.userAlreadyExists = false;
                $scope.error500 = '';
                $scope.showSaveToast();





            },function (res) {

                if (res.status === 409) {
                    $scope.userAlreadyExists = true;
                    

                    return;
                }

                if(res.status === 500){
                    console.dir(res);
                    if(res.data.hasOwnProperty('invalidAttributes')){

                       var errors = '',
                            invalid = res.data.invalidAttributes;


                        for(var i in invalid){
                            errors += '<br>' + invalid[i]["message"];
                        }


                        $scope.error500 = res.data.summary + '<br>' + errors;

                   }else if(res.data.hasOwnProperty('raw')){

                    $scope.error500 = res.data.raw.detail;



                    

                    }else{

                        $scope.error500 = 'Не уникальные значения';
                    }
                }

        });
        };



    });


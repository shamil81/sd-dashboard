angular.module('sd.option',['ui.router','ngMaterial','ngSanitize', 'constants','angularFileUpload'])
    .config(function($stateProvider, $urlRouterProvider){
        $stateProvider
            .state('option', {
                url:'/option',
                views:{
                    'main':{
                        templateUrl:'option/option.tpl.html'
                    }
                },
                data:{
                    pageTitle:'Тарифные опции'
                }



            });
    })

    .controller('OptionCtrl', function($scope, Option, noImageSvg, FileUploader, $auth, $http){
        $scope.elements = [];
        var uploaders = $scope.uploaders = [];






        function createUploader(i){
            var uploader = new FileUploader({
                url: '/api/priceoption/upload',
                headers: {
                    'Authorization': 'JWT ' + $auth.getToken()
                },
                autoUpload: true
            });

            uploader.onBeforeUploadItem = function (item) {
                item.formData.push($scope.elements[i]);

            };

            uploader.onSuccessItem = function (item, res) {
                //angular.copy(res, $scope.elements[i]);
                console.dir(item);
                console.log(res);

                var el = _.find($scope.elements, function(o){
                    return o.title.ru === item.formData[0].title.ru;
                });
                el.image = res.image;
                el.id = res.id;



            };

            $scope.uploaders[i] = uploader;
        }



        Option.get(function(){
            $scope.elements = Option.options;
            $scope.elements.map(function(el, i){

                createUploader(i);


            });
        });

        $scope.save = function(option,frm, index){
            var error = false;
            if(option.title.ru.length === 0){
                frm['title_ru_' + index].$setValidity('required',false);
                error = true;
            }else{
                frm['title_ru_' + index].$setValidity('required',true);

            }
            if(!option.title.en || option.title.en.length === 0 || !/^[a-zA-Z0-9 ]*$/.test(option.title.en)){
                frm['title_en_' + index].$setValidity('required',false);
                error = true;
            }else{
                frm['title_en_' + index].$setValidity('required',true);

            }

            if(option.description.ru.length === 0){
                frm['description_ru_' + index].$setValidity('required',false);
                error = true;
            }else{
                frm['description_ru_' + index].$setValidity('required',true);

            }

            if(!option.description.en || option.description.en.length === 0 || !/^[a-zA-Z0-9 ]*$/.test(option.description.en)){
                frm['description_en_' + index].$setValidity('required',false);
                error = true;
            }else{
                frm['description_en_' + index].$setValidity('required',true);

            }

          /*  if(option.image.length === 0 || option.image === noImageSvg){
                frm['image_' + index].$setValidity('required',false);
                error = true;
            }else{
                frm['image_' + index].$setValidity('required',true);

            }*/

            if(error){
                return;
            }

            if(uploaders[index].queue.length === 0 && option.id < 0){
                delete option.id;
                $http.post('/api/priceoption/upload', option)
                    .then(function(res){
                        option.id = res.data.id;
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });

                return;
            }

            if(option.id > -1){
                $http.put('/api/priceoption/' + option.id, option)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });

                return;
            }





            if(option.id === -1){
                console.log(index);
                uploaders[index].onBeforeUploadItem = function (item) {
                    item.formData = [];
                    console.dir(option);
                    item.formData.push(option);

                };
            }

            uploaders[index].uploadAll();



        };

        function removeEl(i){
            $scope.elements.splice(i,1);
        }

        $scope.destroy = function(el,i){
            if(el.id > -1){
                $http['delete']('/api/priceoption/' + el.id)
                    .then(function(success){
                        removeEl(i);
                    },function(res){
                        $scope.err = true;
                        console.error(res);
                    });

            }else{
                removeEl(i);
            }
        };

        $scope.add = function(){


            $scope.elements.unshift({
                id:-1,
                title:{
                    ru:'',
                    en:''

                },
                description:{
                    ru:'',
                    en:''
                },
                image:''
            });
            createUploader($scope.elements.length - 1);
            console.log($scope.elements.length);

            console.dir($scope.elements[$scope.elements.length - 1]);
        };








    })
    .service('Option', function(Network){
        return {
            options:[],
            get:function(cb){
                var that = this;
                Network.get('priceoption')
                    .then(function(res){
                        that.options = res;
                        cb();
                    });
            },
            save:function(data){
                var that = this;
                if(!angular.equals(this.options, data)){
                    Network.post('priceoption', data)
                        .then(function(res){

                            return;
                        });

                }
            },
            remove:function(o){
                var that = this;

                Network.remove('priceoption', o.id)
                    .then(function(){
                        var i = _.findIndex(that.options, {id: o.id});
                        that.options.splice(i,1);

                    });
            }

        };
    });


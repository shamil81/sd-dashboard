angular.module('constants', [])
    .constant('baseUrl', '/')
    .constant('noImageSvg', '/assets/img/icons/ic_broken_image_black_48px.svg');


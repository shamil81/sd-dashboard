angular.module('sdDashboard', [
        'templates-app',
        'templates-common',
        'ui.router',
        'ngAnimate',
        'ngAria',
        'ngMessages',
        'ngMaterial',
        'satellizer',
        'sd.auth',
        'sd.partner',
        'sd.car',
    'sd.option'
    ])

    .config(function myAppConfig($stateProvider, $urlRouterProvider, $mdThemingProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/');
        $mdThemingProvider.theme('default')
            .primaryPalette('red')
            .accentPalette('yellow');

        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('main', {
                url: '/',
                data: {
                    pageTitle: 'Главная'
                }


                /* controller:function($scope, $auth, $state){
                 if(!$auth.isAuthenticated()){
                 $state.go('login');;
                 }
                 }
                 */
            });


    })


    .run(function ($rootScope, $location) {

        $(document).ready(function (config) {

            setTimeout(function () {
                $('.page-loading-overlay').addClass("loaded");
                $('.load_circle_wrapper').addClass("loaded");
            }, 1000);

        });

    })


    .controller('AppCtrl', function AppCtrl($scope, $rootScope, $location, $auth, $state, AppService) {

        if (!$auth.isAuthenticated()) {
            $state.go('login');
        }

         $scope.openMenu = function ($mdOpenMenu, evt) {
            $mdOpenMenu(evt);
        };

         $scope.logout = function(){
            $auth.logout().then(function(){
                $state.go('login');
            });
        };



        // App wide notifiction

        $scope.$on('showToast', function (e, param) {

            AppService.showToast(param);


        });


        $scope.checkIfOwnPage = function () {

            return _.contains(["/404", "/pages/500", "/login", "/pages/signin", "/pages/signin1", "/pages/signin2", "/pages/signup", "/pages/signup1", "/pages/signup2", "/pages/forgot", "/pages/lock-screen"], $location.path());

        };

        $scope.info = {
            theme_name: "Kimono",
            user_name: "John Doe"
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | Selectdrive';
            }

        });


    })
    .controller("NavCtrl", function ($scope) {

        $scope.navInfo = {
            tasks_number: 5,
            widgets_number: 13
        };

        $scope.toggleAlternativeMenu = function () {
            $('body .page-wrapper').toggleClass('nav-style--alternative');
        };

    })
    .controller('RightNavbarCtrl', function ($scope, $timeout, $mdSidenav, $log) {
        $scope.close = function () {
            $mdSidenav('rightmessages').close()
                .then(function () {
                    $log.debug("close RIGHT is done");
                });
        };

        $scope.toggleRightNavbar = function () {
            $mdSidenav('rightmessages').toggle()
                .then(function () {
                    $log.debug("toggle RIGHT is done");
                });
        };
    })
    .directive("toggleMinNav",
        function ($rootScope) {
            return {
                link: function (scope, ele) {
                    var $content, $nav, $window, Timer, app, updateClass;

                    return app = $("#app"), $window = $(window), $nav = $("#nav-container"), $content = $("#content"), ele.on("click", function (e) {

                        if (app.hasClass("nav-min")) {
                            app.removeClass("nav-min");
                        }
                        else {
                            app.addClass("nav-min");
                            $rootScope.$broadcast("minNav:enabled");
                            e.preventDefault();
                        }

                    }), Timer = void 0, updateClass = function () {
                        var width = $window.width();
                        return (980 > width && !app.hasClass("on-canvas")) ? app.addClass("nav-min") : void 0;
                    }, initResize = function () {
                        var width = $window.width();
                        return 980 > width ? (app.hasClass("nav-min") ? "" : app.addClass("nav-min")) : (app.hasClass("nav-min") ? "" : app.removeClass("nav-min"));
                    }, $window.resize(function () {
                        var t;
                        return clearTimeout(t), t = setTimeout(updateClass, 300);
                    }), initResize();

                }
            };
        })
    .directive("toggleOffCanvas", [
        function () {
            return {
                link: function (scope, ele) {
                    return ele.on("click", function () {
                        return $("#app").toggleClass("on-canvas").toggleClass("nav-min");
                    });
                }
            };
        }
    ]).directive("slimScroll", [
    function () {
        return {
            link: function (scope, ele, attrs) {
                return ele.slimScroll({
                    height: attrs.scrollHeight || "100%"
                });
            }
        };
    }
]).directive("goBack", [
        function () {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$window",
                    function ($scope, $element, $window) {
                        return $element.on("click", function () {
                            return $window.history.back();
                        });
                    }
                ]
            };
        }
    ])
    .controller('ToastCtrl', function ($scope, $mdToast, $mdDialog, text) {
        var isDlgOpen;

        $scope.text = text;

        $scope.closeToast = function () {
            if (isDlgOpen) {
                return;
            }
            $mdToast
                .hide()
                .then(function () {
                    isDlgOpen = false;
                });
        };
        $scope.openMoreInfo = function (e) {
            if (isDlgOpen) {
                return;
            }
            isDlgOpen = true;
            $mdDialog
                .show($mdDialog
                    .alert()
                    .title('More info goes here.')
                    .textContent('Something witty.')
                    .ariaLabel('More info')
                    .ok('Got it')
                    .targetEvent(e)
                )
                .then(function () {
                    isDlgOpen = false;
                });
        };
    })
    .service('AppService', function($mdToast){
        return {
            showToast:function(param){
                var locals = {text: 'Сохранено'};

                switch (param.action) {
                    case 'destroy':
                        locals.text = 'Удалено';
                        break;


                }

                $mdToast.show({
                    hideDelay:3000,
                    position: 'top right',
                    controller: 'ToastCtrl',
                    theme: 'default',
                    locals: locals,
                    templateUrl: 'ui-components/toast.tpl.html',
                    bindToController: true

                });

            }
        };
    })
    .service('Network', function($http, AppService){
        return {
            cache:{},

            // Defaults to serve cached object
            get:function(resource, options, invalidateCache){

                var key = resource + JSON.stringify(options);
                options = options || {};

                if(this.cache[key] && !invalidateCache){
                    return this.cache[key];
                }

                return this.cache[key] = $http({
                    url:'/api/' + resource,
                    method:'GET',
                    params:options
                })
                    .then(function(response){
                        return response.data;
                    });


            },

            post:function(resource, data, noToast){
                return $http.post('/api/' + resource, data)
                    .then(function(response){
                        if(response.status !== 200){
                            return response;
                        }

                        if(noToast){
                            return response.data;
                        }
                        AppService.showToast({action:'save'});
                        return response.data;

                    });
            },

            put:function(resource, data, noToast){
                return $http.put('/api/' + resource, data)
                    .then(function(response){
                        if(noToast){
                            return response.data;
                        }
                        AppService.showToast({action:'save'});
                        return response.data;
                    });
            },

            remove:function(resource,options){
                return $http({
                    url:'/api/' + resource,
                    method:'DELETE',
                    params:options

                })
                    .then(function(response){
                        return response.data;
                    });
            }


        };
    });





angular.module('sd.auth', ['constants', 'satellizer', 'ui.router'])

    .config(function($stateProvider,$authProvider, baseUrl){

        $stateProvider
            .state('login',{
                url:'/login',
                views: {

                    'main':{
                        templateUrl:'auth/login.tpl.html'

                    }

                },
                data:{pageTitle:'Вход'}

            });
        $authProvider.baseUrl = baseUrl;
        $authProvider.loginUrl = '/api/auth';
        $authProvider.signupUrl = '/api/user';
        $authProvider.tokenPrefix = 'selectdrive_a';
        $authProvider.authToken = 'JWT';

    })
    .controller('AuthCtrl', function($scope, $http, $state, $auth){
        if($auth.isAuthenticated()){
            return $state.go('main');
        }
        $scope.wrongCredentials = false;
        $scope.submit = function(frm){
            $scope.wrongCredentials = false;


            if(frm.$invalid){
                return;
            }

            $http.post('/api/admin', $scope.user)

                .then(function(res){
                    $auth.setToken(res.data.token);

                    $state.go('main');


                },
                function(res){

                    if(res.status === 401){

                        $scope.wrongCredentials = true;

                    }

                });



        };

    });


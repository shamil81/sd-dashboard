/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 30/12/15
 */
angular.module('sd.car', ['ui.router', 'angucomplete-alt', 'angularFileUpload','color.picker', 'vAccordion', 'froala','sdDashboard'])
    .config(function ($stateProvider) {


        $stateProvider
            .state('car', {
                url: '/car',
                views: {
                    'main': {
                        templateUrl: 'car/car.tpl.html'
                    }
                },
                data: {
                    pageTitle: 'Автомобили'
                },

              resolve:{
                    carMakers:function (CarService) {

                        return CarService.carMakers();

                    }
              }


            })
            .state('car.class', {
                url: '/class',
                templateUrl: 'car/carCls.tpl.html',
                data: {
                    pageTitle: 'Класс авто'
                }

            })
            .state('car.model', {
                url: '/model/:id',
                templateUrl: 'car/carModel.tpl.html',
                controller:'CarModelCtrl',
                params:{
                    model:null
                },
                data: {
                    pageTitle: 'Модели автомобиля'
                }
            })

            .state('car.body', {
                url: '/body',
                templateUrl: 'car/carBody.tpl.html',
                data: {
                    pageTitle: 'Кузов'
                }

            })
            .state('car.status', {
                url: '/status',
                templateUrl: 'car/carStatus.tpl.html',
                data: {
                    pageTitle: 'Статусы'
                }

            })
            .state('car.color', {
                url: '/color',
                templateUrl: 'car/carColor.tpl.html',
                data: {
                    pageTitle: 'Цвета'
                }

            })
            .state('car.fuel', {
                url: '/fuel',
                templateUrl: 'car/fuel.tpl.html',
                data: {
                    pageTitle: 'Топливо'
                }

            })
            .state('car.wheeldrive', {
                url: '/wheeldrive',
                templateUrl: 'car/carWheelDrive.tpl.html',
                data: {
                    pageTitle: 'Привод колес'
                }

            })
            .state('car.maker', {
                url: '/maker',
                templateUrl: 'car/carMaker.tpl.html',
                controller:'CarMakerCtrl',
                data: {
                    pageTitle: 'Производители'
                }

            });

    })
    .value('froalaConfig', {
    toolbarInline: false,
    language:'ru'
})
    /*  .controller('CarCtrl', function($scope, $http){

     $scope.car = {};
     $scope.success = false;
     $scope.userAlreadyExists = false;
     $scope.error500 = false;


     $scope.handleCarMaker = function(){

     };



     })*/
    .controller('CarCtrl', function ($scope, $state, FileUploader, $auth, $rootScope, $http, CarService) {
        $scope.state = $state;
        CarService.dictionaries()
            .then(function (o) {

                $scope.carBodies = o.carbodystyle;
                $scope.carClasses = o.carclass;
                $scope.selectedMark = $scope.carMakers[0];


            });


        CarService.carMakers()
            .then(function () {
                console.log('carMakers call');
                $scope.carMakers = CarService.makers;
            });





        $scope.goCarClass = function(){
            $state.go('car.class');
        };

        $scope.destroy = function(o, i){

            if(o.item.hasOwnProperty('id') && o.item.id > -1){
                $http['delete']('/api/' + o.url + '/' + o.item.id)
                    .then(function(success){
                        o.arr.splice(i,1);
                    },function(res){
                        $scope.err = true;
                        console.error(res);
                    });

            }else{
                o.arr.splice(i,1);
            }
        };


    })

    .controller('CarModelCtrl', function ($scope, CarService, carMakers, $rootScope, FileUploader, $auth, $http, $state, $stateParams, Network, $timeout) {

        var self = this;






        $scope.froalaOptions = {
            requestHeaders:{
                Authorization:'JWT ' + $auth.getToken()
            },
            imageUploadURL:'/api/carmodel/upload',
            imageUploadParams:{isMedia:true,fileField:'file'},
            imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif']

        };

        var uploader = $scope.uploader = new FileUploader({
            url: '/api/carmaker',
            headers: {
                'Authorization': 'JWT ' + $auth.getToken()
            },
            autoUpload: true
        });





    $scope.carModel = null;
    $scope.carModels = [];

    $scope.carMakers = carMakers;

        if($stateParams.model){

            $scope.carModel = $stateParams.model;


        }


        if($stateParams.id && !$stateParams.model){

          console.dir($scope.carMakers);
            Network.get('carmodel', {id:$stateParams.id})
                .then(function (model) {
                    var i = _.findIndex($scope.carMakers, function (o) {
                        console.dir(model);
                        return o.id === model.maker.id;
                    });
                    model.body = model.body.id;
                    model.carClass = model.carClass.id;

                    $scope.carModel = model;
                    $scope.selectedMark = $scope.carMakers[i];

                    console.dir($scope.selectedMark);



                    $scope.makersList.expand('ml' + i);
                    $scope.carModels = $scope.selectedMark.carModel;



                });

        }


        $scope.$on('makers-list:onReady', function () {
            myAccordion.toggle(yourSuperPaneId);
        });






        $scope.selectMaker = function(maker){

            $scope.selectedMark = maker;
            $scope.carModel = null;
            $scope.carModels = $scope.selectedMark.carModel;

            console.dir($scope.selectedMark);
        };


        self.searchText = '';




        uploader.onBeforeUploadItem = function (item) {
            item.formData.push($scope.selectedMark);

        };

        uploader.onSuccessItem = function (item, res) {
            angular.copy(res, $scope.selectedMark);
            $scope.selectedMark = {
                logo: $scope.selectedMark.logo,
                title: $scope.selectedMark.title

            };
            $scope.carMakers.push($scope.selectedMark);
            $scope.carMakers.sort();
        };

        $scope.$on('angucomplete:create', function (e, search) {

            $scope.selectedMark = {title: search.title};

        });


        $scope.inputChanged = function (str) {
            if (str.length === 0) {
                $scope.selectedMark = null;
            }


        };
        $scope.del = function(model, index){
            $http['delete']('/api/carmodel/' + model.id)
            .then(function(res){
                console.dir(res);
                $scope.carModels.splice(index,1);
            },function (res) {
                if(res.status === 409){
                    $scope.carModels[index].unDeletable = true;
                    $scope.err = res.data;

                }else{
                    $scope.err = res.data;
                }

                console.error(res);
            });
        };


        // Car model
        $rootScope.$on('angucomplete:createModel', function (e, search) {

            $scope.carModel = {title: search.title};

        });


        var modelUploader = $scope.modelUploader = new FileUploader({
            url: '/api/carmodel/upload',
            alias:'image',
            headers: {
                'Authorization': 'JWT ' + $auth.getToken()
            },
            autoUpload: true
        });

        modelUploader.onSuccessItem = function (item, res) {
            angular.extend($scope.carModel, res);
        };





        $scope.submit = function (frm) {
            console.log(frm.$invalid);
            if (frm.$invalid) {
                return;
            }

            if ($scope.carModel.hasOwnProperty('id')) {
                // Update existing car model

                updateCarModel();

            } else {
                addCarModel();

            }


            console.dir($scope.carModel);
        };

        $scope.addAnother = function (frm) {

            if (frm.$invalid) {
                return;
            }

            var copy = {};
            angular.copy($scope.carModel, copy);
            $scope.carModels.unshift(copy);
            delete $scope.carModel.id;

            addCarModel();

        };

        $scope.reset = function(){
            $scope.carModel = null;
        };




        $scope.selectModel = function(maker, model){
            $scope.carModel = model;

            $state.go('car.model', {id: model.id, model:model},{notify: false, reload: false, inherit:false});

           /* $scope.selectedMark = maker;
            $scope.carModels = maker.carModel;
            $scope.carModel = _.find($scope.carModels, function(o){
                return o.id === model.id;
            });*/



            /*cm.produceStart = new Date(cm.produceStart).getFullYear();
            cm.produceEnd = new Date(cm.produceEnd).getFullYear();*/

        };

        function addCarModel() {
            // Append maker
            $scope.carModel.maker = $scope.selectedMark.id;

            Network.post('carmodel', $scope.carModel)
                .then(function (res) {
                    console.dir(res);
                    angular.extend($scope.carModel, res);
                    console.dir($scope.carModels);
                    $scope.carModels.push($scope.carModel);


                }, function (res) {
                    console.error(res);
                    $scope.err = true;
                });
        }

        function updateCarModel() {
            console.dir($scope.carModel);

            $http.put('/api/carmodel/' + $scope.carModel.id, CarService.prepareModel($scope.carModel))
                .then(function (res) {

                }, function (res) {
                    console.error(res);
                    $scope.err = true;
                });
        }


    })

    .controller('CarBodyCtrl', function ($scope, $http) {
       // $scope.carBodies = [];
        $scope.err = false;
        $scope.addBody = function(){
            $scope.carBodies.push({title:{ru:'', en:''}});
        };



        $http.get('/api/carbodystyle')
            .then(function(res){
            //    $scope.carBodies = res.data;
            },function(res){
                $scope.err = true;
                console.error(res);
            });

        $scope.save = function(body,frm){
            console.log(frm.$invalid);
            if(frm.$invalid){
                return;
            }
            if(body.hasOwnProperty('id')){
                $http.put('/api/carbodystyle/' + body.id, body)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});
                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }else{
                $http.post('/api/carbodystyle', body)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});
                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }
        };
    })

    .controller('CarClsCtrl', function ($scope, $http) {


        $scope.carClasses = [];
        $scope.err = false;
        $scope.addCls = function(){
            $scope.carClasses.push({title:{ru:'', en:''}});
        };

        $http.get('/api/carclass')
            .then(function(res){
                $scope.carClasses = res.data;
            },function(res){
                $scope.err = true;
                console.error(res);
            });

        $scope.save = function(cls){
            if(cls.hasOwnProperty('id')){
                $http.put('/api/carclass/' + cls.id, cls)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});
                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }else{
                $http.post('/api/carclass', cls)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }
        };



    })

    .controller('ColorCtrl', function($scope,$http){
        $scope.err = false;
        $scope.colors = [];
        $scope.addColor = function(){
            $scope.colors.push({hex:'', title:{ru:'', en:''}});
        };

        $http.get('/api/carcolor')
        .then(function(res){
            $scope.colors = res.data;
        },function(res){
            $scope.err = true;
            console.error(res);
        });

        $scope.save = function(color){
            if(color.hasOwnProperty('id')){
                $http.put('/api/carcolor/' + color.id, color)
                .then(function(res){
                    $scope.$emit('showToast', {action:'save'});

                }, function(res){
                    $scope.err = true;
                    console.error(res);
                });
            }else{
                $http.post('/api/carcolor', color)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }
        };

    })

    .controller('CarStatusCtrl', function($scope, $http){
        $scope.statuses = [];
        $scope.err = false;
        $scope.addStatus = function(){
            $scope.statuses.push({title:{ru:'', en:''}, description:{ru:'', en:''}});
        };

        $http.get('/api/carstatus')
            .then(function(res){
                $scope.statuses = res.data;
            },function(res){
                $scope.err = true;
                console.error(res);
            });

        $scope.save = function(status){
            if(status.hasOwnProperty('id')){
                $http.put('/api/carstatus/' + status.id, status)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }else{
                $http.post('/api/carstatus', status)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }
        };

    })

    .controller('FuelCtrl', function($scope, $http){
        $scope.fuelList = [];
        $scope.err = false;
        $scope.addFuel = function(){
            $scope.fuelList.push({title:{ru:'', en:''}});
        };

        $http.get('/api/fueltype')
            .then(function(res){
                $scope.fuelList = res.data;
            },function(res){
                $scope.err = true;
                console.error(res);
            });

        $scope.save = function(fuel){
            if(fuel.hasOwnProperty('id')){
                $http.put('/api/fueltype/' + fuel.id, fuel)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }else{
                $http.post('/api/fueltype', fuel)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});
                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }
        };

    })

    .controller('CarWheelDriveCtrl', function($scope, $http){
        $scope.elements = [];
        $scope.err = false;
        $scope.addEl = function(){
            $scope.elements.push({title:{ru:'', en:''}, description:{ru:'', en:''}});
        };

        $http.get('/api/wheeldrive')
            .then(function(res){
                $scope.elements = res.data;
            },function(res){
                $scope.err = true;
                console.error(res);
            });

        $scope.save = function(el){
            if(el.hasOwnProperty('id')){
                $http.put('/api/wheeldrive/' + el.id, el)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }else{
                $http.post('/api/wheeldrive', el)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });
            }
        };

    })
    .controller('CarMakerCtrl', function($scope, CarService, $rootScope, FileUploader, $auth, $http){

        var uploaders = $scope.uploaders = [];


        CarService.carMakers()
            .then(function () {
                $scope.elements = CarService.makers;

                $scope.elements.map(function(el, i){

                    createUploader(i);


                });
            });
        $scope.err = false;
        $scope.addEl = function(){
            $scope.elements.push({title:'', logo:'', id:-1});
            createUploader($scope.elements.length + 1);
        };

        function createUploader(i){
            var uploader = new FileUploader({
                url: '/api/carmaker/upload',
                headers: {
                    'Authorization': 'JWT ' + $auth.getToken()
                },
                autoUpload: false
            });

            uploader.onBeforeUploadItem = function (item) {
                item.formData.push($scope.elements[i]);

            };

            uploader.onSuccessItem = function (item, res) {
                //angular.copy(res, $scope.elements[i]);
                console.dir(item,res);

                var el = _.find($scope.elements, function(o){
                    return o.title === res.title;
                });
               el.logo = res.logo;



            };

            $scope.uploaders.push(uploader);
        }

        function removeEl(i){
            $scope.elements.splice(i,1);
        }


        $scope.destroy = function(el,i){

            if(el.id > -1){
                $http['delete']('/api/carmaker/' + el.id)
                .then(function(success){
                    removeEl(i);
                },function(res){
                    $scope.err = true;
                    console.error(res);
                });

            }else{
                removeEl(i);
            }
        };







        $scope.save = function(el, index){

            console.dir(el);

// Create maker without logo
          if(uploaders[index].queue.length === 0 && el.id < 0){
              delete el.id;
              $http.post('/api/carmaker', el)
                  .then(function(res){
                      el.id = res.data.id;
                      $scope.$emit('showToast', {action:'save'});

                  }, function(res){
                      $scope.err = true;
                      console.error(res);
                  });

              return;
          }

            if(uploaders[index].queue.length === 0 && el.id > -1){
                $http.put('/api/carmaker/' + el.id, el)
                    .then(function(res){
                        $scope.$emit('showToast', {action:'save'});

                    }, function(res){
                        $scope.err = true;
                        console.error(res);
                    });

                return;
            }


            if(el.id === -1){
                uploaders[index].onBeforeUploadItem = function (item) {
                    item.formData = [];
                    item.formData.push(el);

                };
            }


            uploaders[index].uploadAll();



            };

    })

    .service('CarService', function (Network, $q) {
        return {

            makers:[],

            prepareModel:function (model) {
                var o = angular.copy(model);
                delete o.car;
                o.maker = o.maker.id;
                return o;

            },

            addCarMaker:function (maker) {
                that.makers.push(maker);
                that.makers = _.sortBy(that.makers, function(o){
                    return o.title;
                });
            },

            carMakers: function (id) {

                var that = this;
                if(that.makers.length > 0){
                    return $q.resolve(that.makers);
                }

               return Network.get('carmaker')
                    .then(function (makers) {
                        that.makers = makers;
                        that.makers = _.sortBy(that.makers, function(o){
                            return o.title;
                        });
                        return that.makers;

                    });


            },

            dictionaries:function () {
                return $q.all([
                    Network.get('carbodystyle'),
                    Network.get('carmaker'),
                    Network.get('carclass')
                ])
                    .then(function (ar) {
                        var carMakersSorted = _.sortBy(ar[1], function(o){
                            return o.title;
                        });
                        return {
                            carbodystyle:ar[0],
                            carmaker:carMakersSorted,
                            carclass:ar[2]
                        };
                    });
            }





        };
    })
    .filter('appendBefore', function () {
        return function (input) {
            if (input.length > 0) {
                return "\u2192 " + input;
            }
            return input;
        };
    })
    .directive('ngThumb', ['$window', function ($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function (item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function (file) {
                var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function (scope, element, attributes) {
                if (!helper.support) {
                    return;
                }

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) {
                    return;
                }
                if (!helper.isImage(params.file)) {
                    return;
                }

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({width: width, height: height});
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }]);



